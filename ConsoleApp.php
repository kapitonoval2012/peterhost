<?php

namespace MyApp;

require_once 'printable.php';

class ConsoleApp implements \Printable
{
    private $data = [];

    private $responseData = [];

    function __construct()
    {
        // инициализация базы
        $this->dbInit();
        // получаем данные
        $this->loadData();
        // создаем отчет
        $this->createReport();
    }

    /**
     * распаковывает в базу данные
     */
    private function dbInit()
    {
        require_once 'dataSource.php';

        $DB = \Database::getInstance();
        $this->sqlite = $DB->getConnection();
    }

    /**
     * получает информацию из базы
     * @return mixed
     */
    private function loadData()
    {
        $query = "SELECT *,strftime('%m', create_ts) as month ,strftime('%Y', create_ts) as year  FROM [payments] AS d WHERE d.[id] NOT IN (SELECT payment_id FROM [documents] AS d)";
        $st = $this->sqlite->query($query);
        $results = $st->fetchAll();

        $this->data = $results;
    }

    /**
     * форматироване числа в денежный формат
     *
     * @param $number
     * @return string
     */
    private function formatToMoney($number)
    {
        return number_format($number, 2, '.', '');
    }

    /**
     * создает отчет,
     * запоминает в responseData
     *
     * @return mixed
     */
    private function createReport()
    {
        $paymentSortMonth = [];
        foreach ($this->data as $payment) {
            $date = $payment['month'] . '.' . mb_substr($payment['year'], 2, 4);
            $paymentSortMonth[$date][] = $payment;
        }

        foreach ($paymentSortMonth as $date => $paymentsArr) {

            $paymentNum = count($paymentsArr);
            $paymentSum = 0.00;

            if ($paymentNum > 1) {
                foreach ($paymentsArr as $payment) {
                    $paymentSum += $payment['amount'];
                }
            } else {
                $paymentSum = $paymentsArr[0]['amount'];
            }

            $this->responseData[] = [
                'date' => $date,
                'paymentNum' => $paymentNum,
                'paymentSum' => $this->formatToMoney($paymentSum)
            ];
        }
    }

    /**
     * вывод результата
     */
    public function printResult()
    {
//        echo "мм.гг\tколичество платежей\tсумма\n";
        foreach ($this->responseData as $row) {
            echo $row['date'] . "\t" . $row['paymentNum'] . "\t" . $row['paymentSum'] . "\n";
        }
    }

}