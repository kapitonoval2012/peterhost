<?php

abstract class DataSource {
	private $databaseConnection;
	
	public function loadFixtures() 
	{
		$this->createDatabaseConnection();
		$db = $this->getDatabaseConnection();
		$sql = file_get_contents('database.sql');
		$db->exec($sql);
	}

	protected function getDatabaseConnection()
	{
		return $this->databaseConnection;
	}

	private function createDatabaseConnection()
	{
		$this->databaseConnection = new Pdo('sqlite::memory:');
		$this->databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
}


class Database extends DataSource{
    private $_connection;
    private static $_instance;

    public static function getInstance() {
        if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {
        $this->loadFixtures();
        $this->_connection = $this->getDatabaseConnection();
    }

    private function __clone() { }
    // Get  connection
    public function getConnection() {
        return $this->_connection;
    }

}
